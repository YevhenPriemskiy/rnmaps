import React from 'react';
import { Provider } from "react-redux";
import {persistor, store} from './src/app/redux';
import {PersistGate} from 'redux-persist/integration/react'
import { TabsNavigation } from "./src/app/navigation/tabsNavigator";
import { SafeAreaProvider } from "react-native-safe-area-context/src/SafeAreaContext";


const App = () => {
  return (
    <SafeAreaProvider>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <TabsNavigation/>
        </PersistGate>
      </Provider>

    </SafeAreaProvider>
  );
};



export default App;
