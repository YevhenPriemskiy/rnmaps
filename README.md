### WELCOME TO REACT-NATIVE-MAPS Example 

### Some information about project:
It's just an example app which shows how we are using maps and custom form validation.
The app has 2 screens:
* Post screen
* Maps Screen
###Post Screen
Post Screen fetching data from mok-api service [JSON-Placeholder](https://jsonplaceholder.typicode.com/) and shows them on the screen with ability to 
edit rather title and body and update them.That done in a propper React way.So when you click on card,you can edit fields and submit or dismiss chages.If there re no changes,api request won't be called and you ll be returned.If error on back-end happens,the previous data will be resotred and you will notice no chages.The last case is when everything is ok,so the data will update(so,normal behaviour).
Data passing looks like this MainComponents->onMount api Call->If everything s ok,data is splitted beetween components->card with data.
Updating is not re-rendering main list,because the main data not changing,so no side effects are triggered.
WE don't have do update huge ammount of data,because when the screen is mounted we re doing api call which returns as main data.
In cases if you need working with big ammount of data,we will work with local data and update it,whithout refetching it(of course we'll refetch it but not update instantly,data will be compared).

###Map Screen
We have used google maps for android and apple maps for ios(it's not a problem to use google maps),and done all the requirements.You got 2 buttons:
* One for deleting track line
* Second for startring tracking geo-changes

###P.S
Initial location is our office :)

### Main Technologies used in project:
* [React-Native](https://github.com/facebook/react-native)
* [React-Navigation](https://github.com/react-navigation/react-navigation)
* [Redux](https://github.com/reduxjs/redux)
* [Redux-Persist](https://github.com/rt2zz/redux-persist)
* [AsyncStorage](https://github.com/react-native-async-storage/async-storage)
* [React-Native-Vector-Icons](https://github.com/oblador/react-native-vector-icons)
* [React-Native-Touchable-Scale](https://github.com/kohver/react-native-touchable-scale)

### Installation
Download/Clone the project,install dependencies and start the app.
```sh
$ cd RNTest
$ npm install
$ npm run ios or npm run android
```
Or you can download [apk](https://drive.google.com/drive/folders/1whpyhnbLQx4togQHChU_JpA5hgV76Z12?usp=sharing)

### HELP
Android:
> If you having issues with building app just try this:
Download/Clone the projec,install dependencies and start the app.
```sh
$ cd android&&./gradlew clean + delete .gralde folder in ./android
$ cd ..
$ npm run android 
```
IOS:
> If you having issues with building app just try this:
  ```sh
$ cd ios
$ open TestTask.xcodeproj and check in Build Phases if there are no fonts files in Copy Bundle Resources dropdown
$ cd ..
$ npm run ios
```
or
  ```sh
$ cd ios
$ pod update
$ cd ..
$ npm run ios
```


License
----
MIT
