export const REDENTU_GEO = {
  latitude: 49.86234789004158,
  longitude: 24.031117611804337,
  latitudeDelta: 0.0032,
  longitudeDelta: 0.0021
}

export const GEOLOCATION_OPTIONS ={
    enableHighAccuracy: true,
    timeout: 10,
    maximumAge: 15,
    distanceFilter: 15,
}
