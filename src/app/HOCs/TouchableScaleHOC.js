import React from "react";
import TouchableScale from 'react-native-touchable-scale';

export const TouchableScaleHOC = ({
  children,
  callback = () => {},
  ...props
}) => {
  return(
    <TouchableScale activeScale={0.95} tension={50} onPress={callback} {...props}>
      {children}
    </TouchableScale>
  )
}
