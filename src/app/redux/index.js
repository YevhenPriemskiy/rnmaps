import React from 'react';
import {combineReducers, createStore} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Reducers from './reducers'


const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['navigation'],
};

const persistedReducer = persistReducer(
  persistConfig,
  combineReducers(Reducers),
);
const store = createStore(persistedReducer);
const persistor = persistStore(store);

export {store, persistor};
