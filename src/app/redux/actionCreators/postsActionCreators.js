import PostsActions from '../actions/postsActions'
export const getPosts = (payload)=>({
  type: PostsActions.GET_POSTS,
  payload
})
export const setError = ()=>({
  type:PostsActions.SET_ERROR
})
export const setLoading = ()=>({
  type:PostsActions.SET_LOADING
})
export const setPostUpdated = ()=>({
  type:PostsActions.SET_POST_UPDATED
})
