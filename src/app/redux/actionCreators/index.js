import * as PostsActionCreators from './postsActionCreators'

export default {
  ...PostsActionCreators
}
