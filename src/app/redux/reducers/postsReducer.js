import PostsActions from '../actions/postsActions';
const initialState = {
  posts: [],
  loading:true,
  error:false
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case PostsActions.GET_POSTS: {
      return {
        ...state,
        loading: false,
        error: false,
        posts: action.payload,
      };
    }
    case PostsActions.SET_ERROR:{
      return {
        ...state,
        loading: false,
        error: true
      }
    }
    case PostsActions.SET_LOADING:{
      return {
        ...state,
        loading:true
      }
    }
    case PostsActions.SET_POST_UPDATED:{
      return {
        ...state,
        loading: false,
        error: false
      }
    }

    default:
      return state;
  }
};
