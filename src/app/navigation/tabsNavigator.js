import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Posts} from '../screens/Posts';
import {Map} from '../screens/Map';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
const Tab = createBottomTabNavigator();

export const TabsNavigation = () => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        tabBarOptions={{
          keyboardHidesTabBar: true,
        }}>
        <Tab.Screen
          name={'Posts'}
          component={Posts}
          options={{
            tabBarLabel: 'Posts',
            tabBarColor: 'purple',
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons
                name="newspaper-variant"
                color={color}
                size={26}
              />
            ),
          }}
        />
        <Tab.Screen
          name={'Map'}
          component={Map}
          options={{
            tabBarLabel: 'Map',
            tabBarColor: 'purple',
            tabBarIcon: ({color}) => (
              <MaterialCommunityIcons name="earth" color={color} size={26} />
            ),
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};
