import axios from "axios";
import {DEFAULT_API_PATH} from '@env'

export const API_PATH = axios.create({
  baseURL: DEFAULT_API_PATH,
})
