import {API_PATH} from './API_PATH';

export const API = {
  getPosts: (setPosts, setLoading, setError) => {
    setLoading();
    API_PATH.get('/posts')
      .then(({data}) => {
        console.log(data[0])
        setPosts(data)
      })
      .catch(e => setError());
  },
  updatePost: (id, data, setPostUpdated, setError) => {
    API_PATH.put(`posts/${id}`, data)
      .then(({data}) => {
        setPostUpdated();
        console.log(data);
      })
      .catch(e => {
        setError();
        console.log(e);
      });
  },
};
