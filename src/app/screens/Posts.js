import React from 'react';
import {View, FlatList, Animated,KeyboardAvoidingView} from 'react-native';
import {PostCard} from '../components/PostCard';
import {useDimensions} from '../utils/hooks/useDimensions';
import {useFocusEffect} from '@react-navigation/native';
import {API} from '../api/api';
import {useActions} from '../utils/hooks/useActions';
import {useSelector} from 'react-redux';
import {Loader} from '../components/Loader';

export const Posts = () => {
  const [width, height] = useDimensions();
  const {getPosts, setLoading, setError} = useActions();
  const loading = useSelector(state => state.postsReducer.loading);
  const posts = useSelector(state => state.postsReducer.posts);
  const error = useSelector(state => state.postsReducer.error);
  const scrollX = React.useRef(new Animated.Value(0)).current;
  useFocusEffect(
    React.useCallback(() => {
      API.getPosts(getPosts, setLoading, setError);
    }, []),
  );
  const keyExtractor = React.useCallback((item)=>item.id.toString(),[])
  const renderItem = React.useCallback(({item,index})=>{
    const inputRange = [
      (index - 1) * width,
      index * width,
      (index + 1) * width,
    ];
    return (
      <Animated.View
        style={{
          width: width,
          transform: [
            {
              translateY: scrollX.interpolate({
                inputRange,
                outputRange: [0, 20, 0],
              }),
            },
          ],
          opacity: scrollX.interpolate({
            inputRange,
            outputRange: [0.3, 1, 0.3],
          }),
        }}>
        <PostCard data={item} />
      </Animated.View>
    );
  },[])
  return (
    <>
      <View style={{width, height, marginTop: height * 0.35}}>
        {!error && !loading && (
          <Animated.FlatList
            onScroll={Animated.event(
              [{nativeEvent: {contentOffset: {x: scrollX}}}],
              {useNativeDriver: true},
            )}
            horizontal
            pagingEnabled
            showsHorizontalScrollIndicator={false}
            data={posts}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
          />)}
      </View>
      <Loader isLoading={loading} style={{top: height * 0.35}} />
    </>
  );
};
