import React from 'react';
import {Image} from 'react-native';
import MapView from 'react-native-maps';
import {Marker, Polyline} from 'react-native-maps';
import Geolocation from '@react-native-community/geolocation';
import {FindMe} from '../components/FindMe';
import { DeleteTrack } from "../components/ClearTrack";
import {REDENTU_GEO, GEOLOCATION_OPTIONS} from '../../../config';
import { setCurrentGeo } from "../utils/helpers/setCurrentGeo";


export const Map = () => {
  const [isActive, setIsActive] = React.useState(false);
  const [region, setRegion] = React.useState(REDENTU_GEO);
  const watchID = React.useRef();
  const mapRef = React.useRef();
  const [locationTrack, setLocationTrack] = React.useState([]);
  const [fetched, setFetched] = React.useState(false);

  const deleteTrack = React.useCallback(() => {
    setLocationTrack([]);
  }, []);

  React.useEffect(() => {
    if (isActive) {
      watchID.current = Geolocation.watchPosition(
        info => {
          setRegion(setCurrentGeo(info));
          if(fetched) {
            setLocationTrack(prevValue => {
              const region = {
                latitude: info.coords.latitude,
                longitude: info.coords.longitude,
              };
              return prevValue.latitude !== REDENTU_GEO.latitude &&
                prevValue.longitude !== REDENTU_GEO.latitude
                ? [
                  ...prevValue,
                  region,
                ]: [region];
            });
          }
          setFetched(true)
        },
        error => console.log(error),
        GEOLOCATION_OPTIONS,
      );
    }
    if (!isActive) {
      setFetched(false)
      mapRef.current?.animateToRegion(region, 500);
      Geolocation.clearWatch(watchID.current);
    }
    return () => {
      Geolocation.clearWatch(watchID.current);
    };
  }, [isActive]);

  React.useEffect(() => {
    mapRef.current?.animateToRegion(region, 300);
  },[region])

  return (
    <>
      <MapView style={{flex: 1}} initialRegion={region} ref={mapRef} >
        <Polyline coordinates={locationTrack} strokeColor="#000" strokeWidth={6} />
        <Marker coordinate={REDENTU_GEO}>
          <Image
            style={{width: 50, height: 50, borderRadius: 100}}
            source={require('../assets/logo.png')}
          />
        </Marker>
      </MapView>
      <FindMe isActive={isActive} setIsActive={setIsActive} />
      <DeleteTrack callback={deleteTrack} />
    </>
  );
};
