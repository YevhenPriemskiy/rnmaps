import { REDENTU_GEO } from "../../../../config";

export const setCurrentGeo = (info)=>({
  latitude: info.coords.latitude,
  longitude: info.coords.longitude,
  latitudeDelta: REDENTU_GEO.latitudeDelta,
  longitudeDelta: REDENTU_GEO.longitudeDelta,
})
