import React from 'react'
import {View,StyleSheet} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { TouchableScaleHOC } from "../HOCs/TouchableScaleHOC";

export const FindMe = ({isActive,setIsActive})=>{
  return(
    <View style={styles.container}>
      <TouchableScaleHOC callback={()=>setIsActive(!isActive)}>
      <MaterialIcons name={isActive?'my-location':'location-searching'} color={'blue'} size={40} style={{padding:5}}/>
      </TouchableScaleHOC>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 20,
    right: 20,
    borderRadius: 15,
    borderColor: 'rgb(7,71,176)',
    borderWidth: 3,
    backgroundColor: 'rgb(201,222,238)',
  },
})
