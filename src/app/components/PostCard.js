import React from 'react';
import {
  Animated,
  LayoutAnimation,
  Platform,
  UIManager,
  TextInput,
  StyleSheet,
} from 'react-native';
import {useDimensions} from '../utils/hooks/useDimensions';
import {TouchableScaleHOC} from '../HOCs/TouchableScaleHOC';
import {ButtonGroup} from './ButtonGroup';
import {API} from '../api/api';
import {Loader} from './Loader';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}
const AnimatedTextInput = Animated.createAnimatedComponent(TextInput);
export const PostCard = ({data}) => {
  const [width, height] = useDimensions();
  const [title, setTitle] = React.useState(data.title);
  const [body, setBody] = React.useState(data.body);
  const [fallbackStorage, setFallbackStorage] = React.useState({
    title: data.title,
    body: data.body,
  });
  const [loading, setLoading] = React.useState(false);
  const [editable, setEditable] = React.useState(false);
  const AnimatedFlip = React.useRef(new Animated.Value(0)).current;

  const updatePostSuccessCallback = data => {
    setLoading(false);
    setFallbackStorage(data);
    callback(false);
  };
  const updatePostErrorCallback = () => {
    setLoading(false);
    setTitle(fallbackStorage.title);
    setBody(fallbackStorage.body);
  };

  const updatePost = (id, data) => {
    if (
      data.title === fallbackStorage.title &&
      data.body === fallbackStorage.body
    ) {
      callback(false);
    } else {
      setTitle(data.title);
      setBody(data.body);
      setLoading(true);
      API.updatePost(
        id,
        data,
        () => updatePostSuccessCallback(data),
        updatePostErrorCallback,
      );
    }
  };

  const callback = (isUpdatePost = false, id = 0, isCancel = false) => {
    if (isCancel) {
      setTitle(fallbackStorage.title);
      setBody(fallbackStorage.body);
    }
    !isUpdatePost &&
      Animated.timing(AnimatedFlip, {
        toValue: !editable ? 180 : 0,
        duration: 650,
        useNativeDriver: true,
      }).start(() => {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
        setEditable(!editable);
      });
    isUpdatePost && editable && updatePost(id, {title, body});
  };
  const rotation = AnimatedFlip.interpolate({
    inputRange: [0, 180],
    outputRange: [1, -1],
  });
  return (
    <>
      <TouchableScaleHOC
        callback={!editable ? () => callback(false) : null}
        activeScale={!editable ? 0.95 : 1}
        tension={!editable ? 50 : 0}>
        <Animated.View
          style={styles.container(width, rotation)}
          pointerEvents={!editable ? 'none' : 'auto'}>
          <AnimatedTextInput
            value={title}
            onChangeText={text => setTitle(text)}
            editable={editable}
            multiline={true}
            style={styles.title(rotation)}
          />
          <AnimatedTextInput
            onChangeText={text => setBody(text)}
            value={body}
            editable={editable}
            multiline={true}
            style={styles.body(width, rotation)}
          />
          {editable && (
            <ButtonGroup
              AnimatedFlip={AnimatedFlip}
              callback={callback}
              id={data.id}
            />
          )}
        </Animated.View>
      </TouchableScaleHOC>
      <Loader isLoading={loading} />
    </>
  );
};

const styles = StyleSheet.create({
  container: (width, rotation) => ({
    backgroundColor: 'rgb(234,232,232)',
    width: width * 0.95,
    alignSelf: 'center',
    transform: [{scaleX: rotation}],
    borderRadius: 20,
  }),
  title: rotation => ({
    fontSize: 20,
    paddingTop: 16,
    paddingBottom: 25,
    textAlign: 'center',
    color: 'black',
    transform: [{scaleX: rotation}],
  }),
  body: (width, rotation) => ({
    fontSize: 16,
    textAlign: 'center',
    width: width * 0.95,
    alignSelf: 'stretch',
    color: 'black',
    paddingBottom: 20,
    transform: [{scaleX: rotation}],
  }),
});
