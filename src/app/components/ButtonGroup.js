import React from 'react';
import {useDimensions} from '../utils/hooks/useDimensions';
import {
  View,
  Animated,
  TouchableOpacity,
  Text,
  StyleSheet,
} from 'react-native';

export const ButtonGroup = ({AnimatedFlip, callback,id}) => {
  const [width, height] = useDimensions();

  return (
    <>
      <View style={styles.container(width)}>
        <TouchableOpacity onPress={()=>callback(true,id)}>
          <Animated.View style={styles.buttonContainer(width, AnimatedFlip)}>
            <Text
              style={styles.buttonText}>
              SAVE
            </Text>
          </Animated.View>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>callback(false,0,true)}>
          <Animated.View
            style={[
              styles.buttonContainer(width, AnimatedFlip),
              {
                backgroundColor: 'rgb(221,55,55)',
                width: width * 0.25,
              },
            ]}>
            <Text
              style={styles.buttonText}>
              CANCEL
            </Text>
          </Animated.View>
        </TouchableOpacity>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: width => ({
    flexDirection: 'row-reverse',
    width: width * 0.95,
    alignItems: 'center',
    justifyContent: 'space-around',
  }),
  buttonContainer: (width, AnimatedFlip) => ({
    backgroundColor: 'rgba(65,165,65,0.8)',
    opacity: AnimatedFlip.interpolate({
      inputRange: [0, 180],
      outputRange: [0, 1],
    }),
    width: width * 0.6,
    alignSelf: 'center',
    borderRadius: 20,
    marginBottom: 20,
  }),
  buttonText:{
    paddingVertical: 8,
    textAlign: 'center',
    color: 'white',
    transform: [{rotateY: '180deg'}],
  }
});
