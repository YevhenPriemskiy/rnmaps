import React from 'react'
import {View} from 'react-native'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import { TouchableScaleHOC } from "../HOCs/TouchableScaleHOC";

export const DeleteTrack = ({callback})=>{
  return(
    <View style={{position:'absolute',bottom:85,right:20,borderRadius:15,borderColor:'rgb(7,71,176)',borderWidth:3,backgroundColor:'rgb(201,222,238)'}}>
      <TouchableScaleHOC callback={callback}>
        <MaterialIcons name={'delete'} color={'blue'} size={40} style={{padding:5}}/>
      </TouchableScaleHOC>
    </View>
  )
}
