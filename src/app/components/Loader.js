import React from 'react';
import {View, Animated, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';
import {useDimensions} from '../utils/hooks/useDimensions';
export const Loader = ({isLoading, style}) => {
  const [width, height] = useDimensions();
  const [loading, setLoading] = React.useState(false);
  const animateEntrance = React.useRef(new Animated.Value(0)).current;

  React.useEffect(() => {
    isLoading && setLoading(true);
    Animated.timing(animateEntrance, {
      toValue: isLoading ? 1 : 0,
      duration: 300,
      useNativeDriver: true,
    }).start(() => setLoading(isLoading));
  }, [isLoading]);

  return (
    loading && (
      <Animated.View
        style={[
         styles.container(width,height,animateEntrance),
          style,
        ]}>
        <LottieView
          source={require('../assets/lottie/loader.json')}
          autoPlay={true}
          style={{width: 200, height: 200}}
        />
      </Animated.View>
    )
  );
};

const styles = StyleSheet.create({
  container:(width,height,animateEntrance)=>({
    opacity: animateEntrance,
    position: 'absolute',
    alignSelf: 'center',
    top: -height * 0.001,
    right: width * 0.25,
  })
})
